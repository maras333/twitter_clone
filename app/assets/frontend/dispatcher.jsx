import React from 'react';
import ReactDOM from 'react-dom';

import Flux from 'flux';

export default new Flux.Dispatcher();