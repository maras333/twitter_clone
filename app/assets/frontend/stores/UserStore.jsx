import React from 'react';
import ReactDOM from 'react-dom';

import AppDispatcher from "../dispatcher.jsx";
import ActionTypes from "../constants.jsx";
import AppEventEmitter from "./AppEventEmitter.jsx";


let _users = [];
let _followedIds = [];
let _usersToUnfollow = [];


class UserEventEmitter extends AppEventEmitter
{
	getAll(){
		console.log(_followedIds);
		return _users.map(user =>{
			user.following = _followedIds.indexOf(user.id) >=0;
			return user; 
		})
	}

	getAllToUnfollow(){
		console.log(_followedIds);
		return _usersToUnfollow.map(user =>{
			user.following = _followedIds.indexOf(user.id) >=0;
			return user; 
		})
	}

};

let UserStore = new UserEventEmitter();

AppDispatcher.register( action => {
	//action.actionType === RECEIVED_TWEETS
	
	switch(action.actionType) {
		case ActionTypes.RECEIVED_USERS:
		console.log(4, "UserStore: UsersReceived!");
		_users = action.usersToFollow;
		UserStore.emitChange();
		break;

		case ActionTypes.RECEIVED_ONE_FOLLOWER:
		console.log(4, "UserStore: ONE FOLLOWER!"); 
		_followedIds.push(action.rawFollower.user_id);
		UserStore.emitChange();
		break;

		case ActionTypes.RECEIVED_USERS_TO_UNFOLLOW:
		_usersToUnfollow = action.usersToUnfollow;
		UserStore.emitChange(); 
		break;

		case ActionTypes.RECEIVED_ONE_UNFOLLOWER:
		_followedIds.splice(action.rawUnfollower.user_id);
		UserStore.emitChange(); 
		break;

		default:
		//no op
	}
});

export default UserStore;