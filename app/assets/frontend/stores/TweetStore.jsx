import React from 'react';
import ReactDOM from 'react-dom';

import AppDispatcher from "../dispatcher.jsx";
import ActionTypes from "../constants.jsx";
import AppEventEmitter from "./AppEventEmitter.jsx";

let _tweets = [];


export default class TweetEventEmitter extends AppEventEmitter
{
	getAll(){
		return _tweets.map(tweet=>{
			tweet.existFrom = moment(tweet.created_at).fromNow();
			return tweet;
		});
	}
};

let TweetStore = new TweetEventEmitter();


AppDispatcher.register( action => {
	//action.actionType === RECEIVED_TWEETS
	
	switch(action.actionType) {
		case ActionTypes.RECEIVED_TWEETS:
		console.log(4, "TweetStore_all_tweets");
			_tweets = action.rawTweets;
			TweetStore.emitChange();
			break;

		case ActionTypes.RECEIVED_ONE_TWEET:
		console.log(4, "TweetStore_received_one_tweet");
			_tweets.unshift(action.rawTweet);
			TweetStore.emitChange();
			break;

		default:
		//no op
	}
});

export default TweetStore;

	// formattedList(tweetList){
	// 	let formatted = tweetList.map(tweet=>{
	// 		tweet.existFrom = moment(tweet.created_at).fromNow();
	// 		return tweet;
	// 	});
	// 	return{tweetsList: formatted};
	// }