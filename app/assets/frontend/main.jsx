
import React from 'react';
import ReactDOM from 'react-dom';
import Index from './components/Index.jsx';
import Follow from './components/Follow.jsx';
import Unfollow from './components/Unfollow.jsx';

// using an ES6 transpiler, like babel
import { Router, Route, Link } from 'react-router';

class App extends React.Component{
	render(){
		return(
			<div>
			 {this.props.children}
			</div>
		);
	}
}

let documentReady = () => {
	let reactNode = document.getElementById('react');
	if(reactNode){ 
		ReactDOM.render(
			<Router>
				<Route component={App}>  
					<Route path="/" component={Index}/>
					<Route path="/follow" component={Follow}/>
					<Route path="/unfollow" component={Unfollow}/>

				</Route>
			</Router>
			,reactNode		
		);
	}
};

$(documentReady);

