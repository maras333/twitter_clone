import React from 'react';
import ReactDOM from 'react-dom';

import API from "../API.jsx";

export default {     //Tweet actions wich send a message to API module to get all the tweets
	getAllTweets(){
		console.log(1, "TweetActions_getALL!");
		API.getAllTweets(); 
	},

	sendTweet(body){   //sending a message to API utility
		console.log(1, "TweetActions_createONE!");
		API.createTweet(body);

	}
}