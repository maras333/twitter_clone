import React from 'react';
import ReactDOM from 'react-dom';

import API from "../API.jsx";

export default {

		getUsersToFollow(){
		console.log(1, "UserActions!, getUsersToFollow!");
		API.getUsersToFollow(); 
	},

		followUser(userId){
		API.followUser(userId);
	},

		getUsersToUnfollow(){
		console.log(1, "UserActions! getUsersToUnfollow");
		API.getUsersToUnfollow();
	},

		unfollowUser(userId){
		API.unfollowUser(userId);
	}
}