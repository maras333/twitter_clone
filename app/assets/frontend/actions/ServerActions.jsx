import React from 'react';
import ReactDOM from 'react-dom';

import ActionTypes from "../constants.jsx";
import AppDispatcher from "../dispatcher.jsx";
export default {
	receivedTweets(rawTweets){ // ServerAction told the Dispatcher about these data , Dispatcher labeled the data and dispatched it	
		console.log(3, "ServerAction.receivedTweets");
		AppDispatcher.dispatch({
			actionType: ActionTypes.RECEIVED_TWEETS,
			rawTweets: rawTweets
		}) 
	},

	receivedOneTweet(rawTweet){ // ServerAction told the Dispatcher about these data , Dispatcher labeled the data and dispatched it	
		console.log(3, "ServerAction.receivedOneTweet");
		AppDispatcher.dispatch({
			actionType: ActionTypes.RECEIVED_ONE_TWEET,
			rawTweet: rawTweet
		}) 
	},

	receivedUsersToFollow(usersToFollow){ // ServerAction told the Dispatcher about these data , Dispatcher labeled the data and dispatched it	
		console.log(3, "ServerAction.usersToFollow");
		AppDispatcher.dispatch({
			actionType: ActionTypes.RECEIVED_USERS,
			usersToFollow: usersToFollow
		}) 
	},

	receivedOneFollower(rawFollower){
		AppDispatcher.dispatch({
			actionType: ActionTypes.RECEIVED_ONE_FOLLOWER,
			rawFollower: rawFollower	
		}) 
	},

	receivedUsersToUnfollow(usersToUnfollow){ 
		AppDispatcher.dispatch({
			actionType: ActionTypes.RECEIVED_USERS_TO_UNFOLLOW,
			usersToUnfollow: usersToUnfollow	
		}) 
	},

	receivedOneUnfollower(rawUnfollower){
		AppDispatcher.dispatch({
			actionType: ActionTypes.RECEIVED_ONE_UNFOLLOWER,
			rawUnfollower: rawUnfollower	
		}) 
	},
}