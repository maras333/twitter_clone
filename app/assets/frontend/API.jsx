import React from 'react';
import ReactDOM from 'react-dom';

import ServerActions from "./actions/ServerActions.jsx";


export default {		//it send all tweets and created a ServerAction and passed along the data
	getAllTweets() {

		console.log(2, "API.getAllTweets");
		$.get("/tweets")
		.success(rawTweets => ServerActions.receivedTweets(rawTweets))
		.error(error => console.log(error));
	},

	createTweet(body) { //write one tweet to /tweets and send it to ServerAction and pass this tweet

		console.log(2, "API.createTweet");
		$.post("/tweets", {body})
		.success(rawTweet => ServerActions.receivedOneTweet(rawTweet))
		.error(error => console.log(error));
	},

	getUsersToFollow(){
		console.log(8, "API.getUsersToFollow");
		$.get("/followers/random")
		.success(usersToFollow => ServerActions.receivedUsersToFollow(usersToFollow))
		.error(error => console.log(error));
	},

	followUser(userId){
		$.post("/followers", {user_id: userId})
		.success(rawFollower => ServerActions.receivedOneFollower(rawFollower))
		.error(error => console.log(error));
	},

	getUsersToUnfollow(){
		console.log(9, "API.getUsersToUnfollow");
		$.get("/followers")
		.success(getUsersToUnfollow => ServerActions.receivedUsersToUnfollow(getUsersToUnfollow))
		.error(error => console.log(error));
	},

    unfollowUser(userId){
    console.log (13, "API.unfollowUser");   
    $.ajax({
      url: '/followers/unfollow',
      method: 'DELETE',
      data: { user_id: userId }
    }).done(unfollowUser => ServerActions.receivedOneUnfollower(unfollowUser))
      .fail(error => console.log(error));

	},




}