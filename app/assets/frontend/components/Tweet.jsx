import React from 'react';
import ReactDOM from 'react-dom';


export default class Tweet extends React.Component{

	render(){
		let tweetName = this.props.name;
		let tweetBody = this.props.body;

		return(
		<li className="collection-item avatar">
			<img className="circle" src={this.props.gravatar} />
			<span className="title">{tweetName}</span>
			<time> {this.props.existFrom} </time>
			<p>{tweetBody}</p>
		</li>
		);
	}
}

