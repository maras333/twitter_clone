import React from 'react';
import ReactDOM from 'react-dom';
import {Link} from 'react-router';

import TweetBox from "./TweetBox.jsx";
import TweetList from "./TweetList.jsx";
import TweetStore from "../stores/TweetStore.jsx";

import TweetActions from "../actions/TweetActions.jsx";




let getAppState = () => {
	return {tweetsList: TweetStore.getAll() };  //return all tweets from TweetStore
};

let mockTweets = [
	{id: 1, name: 'Maras Czyż', body: 'My first tweet'},
	{id: 2, name: 'Maras Czyż', body: 'My second tweet'},
	{id: 3, name: 'Maras Czyż', body: 'My third tweet'},
	{id: 4, name: 'Maras Czyż', body: 'My fourth tweet'},
	{id: 5, name: 'Maras Czyż', body: 'My fifth tweet'},	
];

export default class Index extends React.Component {

	constructor(props){
		super(props);
		this.state=getAppState();
		this._onChange = this._onChange.bind(this);
	}



	

	componentDidMount() {
		TweetActions.getAllTweets(); 
		TweetStore.addChangeListener(this._onChange); 
		//after mounting addChangeListener aktivated and call a callback
	}												//_onChange which set a state again using getAppState which
													 //use data directly from TweetStore
	componentWillUnmount() {
		TweetStore.removeChangeListener(this._onChange);
	}

	_onChange(){
		console.log(5, "Main._onChange");
		this.setState(getAppState());
	}

	render(){
		return(
			<div className="container">
				<div>
					<Link to="/follow">Who to follow?</Link>
				</div>
				<div>
					<Link to="/unfollow">Who to unfollow?</Link>
				</div>
				<TweetBox />
				<TweetList tweets={this.state.tweetsList}/>
			</div>
		);
	}
}
