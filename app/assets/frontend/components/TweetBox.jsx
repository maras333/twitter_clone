import React from 'react';
import ReactDOM from 'react-dom';

import TweetActions from "../actions/TweetActions.jsx";

export default class TweetBox extends React.Component{

	sendTweet(event){
		event.preventDefault();
		//this.props.sendTweet(this.refs.tweetTextArea.value);
		console.log(2, "Tweet_box_is_sending!");
		TweetActions.sendTweet(this.refs.tweetTextArea.value);
		this.refs.tweetTextArea.value = '';

	}

	render(){
		return(
			<div className="row">
		    <form onSubmit={this.sendTweet.bind(this)}>
		        <div className="input-field">
		          <textarea ref="tweetTextArea" id="textarea1" className="materialize-textarea"></textarea>
		          <label htmlFor="textarea1">What's up?</label>
		          <button type="submit" className="btn waves-effect waves-light right" type="submit" name="action">Tweet</button>
		        </div>
		    </form>
	    </div>
		);
	}
}