import React from 'react';
import UserStore from '../stores/UserStore.jsx'

import UserActions from "../actions/UserActions.jsx";
import {Link} from "react-router";

let getAppState = () =>{
	return {users: UserStore.getAllToUnfollow()};  //get all users to unfollow DIRECTLY FROM FROM STORE!!!
}

export default class Unfollow extends React.Component{

	constructor(props){
		super(props);
		this.state = getAppState();
		this._onChange = this._onChange.bind(this); 
	}



	componentDidMount() {
		UserActions.getUsersToUnfollow(); //after mounting to invoke function which take UsersToFollow FROM SERVER!!!
		UserStore.addChangeListener(this._onChange);  //after mounting addChangeListener aktivated and call a callback
	}												//_onChange which set a state again using getAppState which
													 //use data directly from TweetStore
	componentWillUnmount() {
		UserStore.removeChangeListener(this._onChange);
	}

	_onChange(){
		console.log(5, "Main._onChange");
		this.setState(getAppState());
	}

	unfollowClasses(following){
		return "secondary-content btn-floating " + (following ? "green" : "grey");	
	}

	unfollowUser(userId){
		UserActions.unfollowUser(userId);
	}

	render(){
		let users = this.state.users.map(user =>{
			return(
				<li key={user.id} className="collection-item avatar">
					<img src={user.gravatar} className="circle" />
					<span className="title"> {user.name} </span>
					<a className = {this.unfollowClasses(user.following)} onClick={this.unfollowUser.bind(this, user.id)}>
						<i className = "material-icons">person-pin</i>
					</a>
				</li>)
		})		
		return(
			
			<div>
				<h3> Who to unfollow? </h3>
				<ul className = "collection">
					{users}
				</ul>
				<Link to="/">Back</Link>
			</div>
			
			);
	}

}