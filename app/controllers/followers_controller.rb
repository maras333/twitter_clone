class FollowersController < ApplicationController

	def index
		render json: User.following(current_user.id)
	end

	def random
		render json: User.who_to_follow(current_user.id)
		
	end

	def unfollow
	  follower = Follower.find_by(
	    user_id: params[:user_id],
	    followed_by: current_user.id
	  )
	  follower.destroy

	  render json: follower
	end



	def create
		follower = Follower.create(user_id: params[:user_id],
		 						   followed_by: current_user.id)

		render json: follower
			
	end
end
