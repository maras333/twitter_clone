class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  def as_json(options={}) 	
	{id: id, name: display_name, gravatar: gravatar}   #forced serialized json
		
  end

  def display_name
  	first_name.present? ? "#{first_name} #{last_name}" : email
  end

  def name
  	first_name.present? ? "#{first_name} #{last_name}" : email
  end

  def gravatar
  		hash = Digest::MD5.hexdigest(email.downcase)
		"http://www.gravatar.com/avatar/#{hash}"
  end

  def self.who_to_follow(current_user_id)     # I DONT GET IT!!! REMEMBER TO CHECK THIS OUT!!!!
  	where(["id != :current_user_id and not exists (         
  		select 1 from followers
  		where user_id = users.id
  		and followed_by = :current_user_id)", {current_user_id: current_user_id }]).all
  	 #THIS LIST ALL RECORDS IN THE USERS TABLE EXCLUDING CURRENT USERS RECORDS AND EXCLUDING ALL USERS WHO
  	 #EXIST IN  THE LIST OF USERS THE CURRENT USER IS FOLLOWINNG
  	
  end

  def self.following(current_user_id)   # LIST OF USERS WHO THE CURRENT USER IS FOLLOWING
  	where(["id != :current_user_id and exists (
  		select 1 from followers
  		where followed_by = :current_user_id
  		and user_id = users.id)", {current_user_id: current_user_id}]).all
  	
  end
end
